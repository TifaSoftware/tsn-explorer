﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace TSN
{
    public partial class loginForm : Form
    {
        public event EventHandler Authenticated;
        public loginForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TifaBrowser tb = new TifaBrowser();
            tb.MdiParent = MdiParent;
            tb.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var mainform = (MainForm)MdiParent;
            mainform.creds = new Credentials(textBox1.Text, textBox2.Text);
            //TifaBrowser tb = new TifaBrowser(textBox1.Text, textBox2.Text);
            //tb.MdiParent = MdiParent;
            //tb.toLogin = true;
            //tb.Show();
            //tb.loginSuccess += new EventHandler(tb_loginSuccess);
            //tb.loginFail += new EventHandler(tb_loginFail);
            string contents;
            using (var wc = new System.Net.WebClient())
            {
                string postData = "username=" + textBox1.Text + "&password=" + textBox2.Text + "&isTSN=true";
                //System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                //byte[] bytes = encoding.GetBytes(postData);
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                contents = wc.UploadString("http://tifasoftware.z86.ru/tsn/login.php", postData);
            }
            if (contents == "true")
            {
                this.Close();
                Authenticated(this, new EventArgs());
            }
            else {
                MessageBox.Show("Invalid Credentials");
            }
        }

        void tb_loginFail(object sender, EventArgs e)
        {
            MessageBox.Show("Invalid Credentials");
        }

        void tb_loginSuccess(object sender, EventArgs e)
        {
            this.Close();
            Authenticated(this, new EventArgs());
            
        }

        private void loginForm_Load(object sender, EventArgs e)
        {

        }
    }
}
