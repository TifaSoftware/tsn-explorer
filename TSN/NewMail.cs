﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace TSN
{
    public partial class NewMail : Form
    {
        public NewMail()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Compose Reply Mail
        /// </summary>
        /// <param name="subject">Subject</param>
        /// <param name="to">Recipient</param>
        public NewMail(string subject, string to)
        {
            InitializeComponent();
            toolStripTextBox1.Text = to;
            toolStripTextBox2.Text = subject;
        }

        public NewMail(string subject) 
        {
            InitializeComponent();
            
            toolStripTextBox2.Text = subject;
        }

        public void SetRTF(string rtf) 
        {
            if (rtf.TrimStart().StartsWith(@"{\rtf", StringComparison.Ordinal))
            {
                tifaWorksEditor1.Rtf = rtf;
            }
        }

        private void NewMail_Load(object sender, EventArgs e)
        {
            
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            using (var wc = new System.Net.WebClient())
            {
                var mf = (MainForm)MdiParent;
                Credentials creds = mf.creds;
                string postData = "sender=" + creds.Username + "&reciever=" + toolStripTextBox1.Text + "&date=" + Base64.Encode(DateTime.Today.ToString("r")) + "&subject=" + toolStripTextBox2.Text + "&body=" + Base64.Encode(tifaWorksEditor1.Rtf);
                //MessageBox.Show(postData);
                //System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                //byte[] bytes = encoding.GetBytes(postData);
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                var s = wc.UploadString("http://tifasoftware.z86.ru/tsn/sendmail.php", postData);
                if (s == "sent")
                {
                    this.Close();
                }
                else 
                {
                    MessageBox.Show("Cannot send message. \n" + s);
                }

            }
        }
    }
}
