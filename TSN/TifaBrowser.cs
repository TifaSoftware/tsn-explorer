﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TSN
{
    public partial class TifaBrowser : Form
    {
        //int gracePeriod = 0;
        public event EventHandler loginSuccess;
        public event EventHandler loginFail;
        byte[] bytes;
        bool toRegister = false;
        public bool toLogin = false;
        public TifaBrowser(string user, string password)
        {
            InitializeComponent();
            string postData = "username="+user+"&password="+password+"&isTSN=true";
            System.Text.Encoding encoding = System.Text.Encoding.UTF8;
            bytes = encoding.GetBytes(postData);
        }
        public TifaBrowser() 
        {
            InitializeComponent();
            toRegister = true;
            panel2.Visible = false;
        }

        private void TifaBrowser_Load(object sender, EventArgs e)
        {
            if (toRegister)
            {
                webBrowser1.Navigate("http://tifasoftware.z86.ru/register.php");
            }
            else
            {
                webBrowser1.Navigate("http://tifasoftware.z86.ru/login.php", string.Empty, bytes, "Content-Type: application/x-www-form-urlencoded");
            }
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            toolStripTextBox1.Text = e.Url.AbsoluteUri;
            if (toRegister || toLogin)
            {
                if (e.Url.AbsoluteUri == "http://tifasoftware.z86.ru/index.php")
                {
                    loginSuccess(this, new EventArgs());
                    this.Close();
                }
            }
            else {
                if (e.Url.AbsoluteUri == "http://tifasoftware.z86.ru/index.php")
                {
                    loginSuccess(this, new EventArgs());
                } else if (e.Url.AbsoluteUri == "http://tifasoftware.z86.ru/login.php")
                {
                    loginFail(this, new EventArgs());
                        this.Close();
                    
                }
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            webBrowser1.Navigate("http://tifasoftware.z86.ru/login.php", string.Empty, bytes, "Content-Type: application/x-www-form-urlencoded");
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            webBrowser1.Refresh();
        }
    }
}
