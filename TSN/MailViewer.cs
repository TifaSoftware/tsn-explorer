﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using Newtonsoft.Json;

namespace TSN
{
    public partial class MailViewer : Form
    {
        Dictionary<ListViewItem, string> Mail;
        Dictionary<ListViewItem, string> Senders;
        public MailViewer()
        {
            InitializeComponent();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            NewMail nm = new NewMail();
            nm.MdiParent = MdiParent;
            nm.Show();
        }

        private void MailViewer_Load(object sender, EventArgs e)
        {
            RefreshM();
        }

        private void RefreshM() 
        {
            listView1.Items.Clear();
            listView1.Sorting = SortOrder.None;
            Mail = new Dictionary<ListViewItem, string>();
            Senders = new Dictionary<ListViewItem, string>();
            using (var wc = new System.Net.WebClient())
            {
                var mf = (MainForm)MdiParent;
                Credentials creds = mf.creds;
                string postData = "username=" + creds.Username + "&password=" + creds.Password;
                //System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                //byte[] bytes = encoding.GetBytes(postData);
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                List<Dictionary<string, string>> mail = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(wc.UploadString("http://tifasoftware.z86.ru/tsn/getmail_indx.php", postData));
                foreach (Dictionary<string, string> m in mail)
                {
                    var item = listView1.Items.Add(m["subject"]);
                    item.SubItems.Add(m["sender"]);
                    Senders.Add(item, m["sender"]);
                    item.SubItems.Add(ProcessDate(DateTime.Parse(m["date"])));
                    Mail.Add(item, m["id"]);
                }
            }
            listView1.Sorting = SortOrder.Descending;
            listView1.Sort();
        }

        String ProcessDate(DateTime dt) 
        {
            if (dt.Day == DateTime.Today.Day && dt.Month == DateTime.Today.Month && dt.Year == DateTime.Today.Year) {
                return "Today";
            }
            else if (dt.Day + 1 == DateTime.Today.Day && dt.Month == DateTime.Today.Month && dt.Year == DateTime.Today.Year)
            {
                return "Yesterday";
            }
            else {
                return dt.ToShortDateString();
            }
            return "";
        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1){
                    using (var wc = new System.Net.WebClient())
                    {
                        var mf = (MainForm)MdiParent;
                        Credentials creds = mf.creds;
                        string postData = "username=" + creds.Username + "&password=" + creds.Password + "&id=" + Mail[listView1.SelectedItems[0]];
                        //MessageBox.Show(postData);
                        //System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                        //byte[] bytes = encoding.GetBytes(postData);
                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                        var s = wc.UploadString("http://tifasoftware.z86.ru/tsn/getmail.php", postData);
                        try
                        {
                            richTextBox1.Rtf = Base64.Decode(s);
                        }
                        catch (Exception erf)
                        {
                            richTextBox1.Text = "Mail could not be retrieved";
                        }

                    }

               
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            RefreshM();
        }
        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                using (var wc = new System.Net.WebClient())
                {
                    var mf = (MainForm)MdiParent;
                    Credentials creds = mf.creds;
                    string postData = "username=" + creds.Username + "&password=" + creds.Password + "&id=" + Mail[listView1.SelectedItems[0]];
                    //MessageBox.Show(postData);
                    //System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                    //byte[] bytes = encoding.GetBytes(postData);
                    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                    var s = wc.UploadString("http://tifasoftware.z86.ru/tsn/deletemail.php", postData);
                    if (s == "ok")
                    {
                        richTextBox1.Clear();
                        RefreshM();
                    }
                    else
                    {
                        MessageBox.Show("Cannot Delete Message : " + s);
                    }


                }


            }
            else if (listView1.SelectedItems.Count > 1)
            {
                using (var wc = new System.Net.WebClient())
                {
                    var mf = (MainForm)MdiParent;
                    Credentials creds = mf.creds;
                    foreach (ListViewItem i in listView1.SelectedItems)
                    {
                        string postData = "username=" + creds.Username + "&password=" + creds.Password + "&id=" + Mail[i];
                        //MessageBox.Show(postData);
                        //System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                        //byte[] bytes = encoding.GetBytes(postData);
                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";

                        var s = wc.UploadString("http://tifasoftware.z86.ru/tsn/deletemail.php", postData);
                        if (s != "ok")
                        {
                            MessageBox.Show("Cannot Delete Message : " + s);
                        }
                    }


                }
                richTextBox1.Clear();
                RefreshM();
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                String sub = listView1.SelectedItems[0].Text;
                if (sub.ToLower().StartsWith("re: ") == false)
                {
                    sub = "Re: " + sub;
                }
                NewMail nm = new NewMail(sub, Senders[listView1.SelectedItems[0]]);
                nm.MdiParent = MdiParent;
                nm.Show();
            }
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                String sub = listView1.SelectedItems[0].Text;
                if (sub.ToLower().StartsWith("fwd: ") == false)
                {
                    sub = "Fwd: " + sub;
                }
                NewMail nm = new NewMail(sub);
                nm.SetRTF(richTextBox1.Rtf);
                nm.MdiParent = MdiParent;
                nm.Show();
            }
        }
    }
}
