﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Net;

namespace TSN
{
    public partial class MainForm : Form
    {
        public Dictionary<string, string> userinfo;
        public Credentials creds;
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            loginForm lf = new loginForm();
            lf.MdiParent = this;
            lf.Authenticated += new EventHandler(lf_Authenticated);
            lf.Show();
            toolStrip1.Visible = false;

        }

        void lf_Authenticated(object sender, EventArgs e)
        {
            toolStrip1.Visible = true;
            using (var wc = new System.Net.WebClient())
            {
                string postData = "username=" + creds.Username; // + "&password=" + creds.Password + "&isTSN=true"
                //System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                //byte[] bytes = encoding.GetBytes(postData);
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                userinfo = JsonConvert.DeserializeObject<Dictionary<string, string>>(wc.UploadString("http://tifasoftware.z86.ru/tsn/getinfo.php", postData));
            }
            if (userinfo["lastname"] == "")
            {
                toolStripDropDownButton1.Text = userinfo["firstname"];
            }
            else {
                toolStripDropDownButton1.Text = userinfo["firstname"] + " "+ userinfo["lastname"];
            }
            
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            TifaBrowser tb = new TifaBrowser(creds.Username, creds.Password);
            tb.MdiParent = this;
            tb.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form f in MdiChildren)
            {
                f.Close();
            }
            if (MdiChildren.Length == 0)
            {
                loginForm lf = new loginForm();
                lf.MdiParent = this;
                lf.Authenticated += new EventHandler(lf_Authenticated);
                lf.Show();
                toolStrip1.Visible = false;
                creds = null;
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            MailViewer mv = new MailViewer();
            mv.MdiParent = this;
            mv.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Tifa Software Network Explorer Beta v0.6" + Environment.NewLine + "Licensed Under the TSL." + Environment.NewLine + "http://tifasoftware.z86.ru/");
        }
    }
}
