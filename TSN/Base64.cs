﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TSN
{
    class Base64
    {
        public static string Encode(string rtf) 
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(rtf);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Decode(string base64) 
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
