﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TSN
{
    public class Credentials
    {
        public Credentials(string username, string password) 
        {
            _username = username;
            _password = password;
        }

        private string _username;
        public string Username 
        {
            get {
                return _username;
            }
        }
        private string _password;
        public string Password
        {
            get
            {
                return _password;
            }
        }
    }
}
